package com.zuitt.activity;

import java.util.ArrayList;

public class Phonebook extends Contact{

    ArrayList<String> contacts = new ArrayList<String>();

    public Phonebook() {
        super();
    }

    public Phonebook(String name, String contactNumber, String address) {
        super(name, contactNumber, address);
    }

    public void setName(String name) {
        this.name = name;
        contacts.add(name);
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
        contacts.add(contactNumber);
    }

    public void setAddress(String address) {
        this.address = address;
        contacts.add(address);
    }

    public void phoneList() {
        for(int x = 0; x < contacts.size(); x += 3) {
            System.out.println("------------------------------");
            System.out.println(contacts.get(x));
            System.out.println("------------------------------");

            for(int y = 1; y < contacts.size(); y += 3) {
                System.out.println(contacts.get(x) + " has the following registered number:");
                System.out.println(contacts.get(y));
            }

            for(int z = 2; z < contacts.size(); z += 3) {
                System.out.println(contacts.get(x) + " has the following registered address:");
                System.out.println(contacts.get(z));
            }
        }
    }
}
