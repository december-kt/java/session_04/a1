package com.zuitt.activity;

public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        phonebook.setName("John Doe");
        phonebook.setContactNumber("+639152468596");
        phonebook.setAddress("Quezon City");

        phonebook.setName("Jane Doe");
        phonebook.setContactNumber("+639162148573");
        phonebook.setAddress("Caloocan City");

        phonebook.phoneList();

    }
}
